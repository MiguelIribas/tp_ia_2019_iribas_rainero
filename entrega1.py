from simpleai.search import SearchProblem, breadth_first, depth_first, greedy, astar
from simpleai.search.viewers import WebViewer, BaseViewer, ConsoleViewer

filas_tablero = 5
columnas_tablero = 5
isla_mapa = (2,0)
isla_pirata = (filas_tablero,columnas_tablero)

#  Pasar de tupla a lista
def tupla_a_lista(t):
    return [list(row) for row in t]

#  Pasar de lista a tupla
def lista_a_tupla(t):
    return tuple(tuple(row) for row in t)

class ProblemaPiratas(SearchProblem):

    def is_goal(self, state):
        for pirata in state[1]:
            if pirata[0]==isla_pirata and pirata[1] == 1: #Si esta en la isla pirata y tiene el mapa.
                return True
        return False

    def actions(self,state):
        acciones=[]
        estado = state
        for pirata in estado[1]:
            fila_pirata, columna_pirata = pirata[0]
            if (fila_pirata+1)<= filas_tablero:
                accion = (pirata,(1,0)) #  Mover abajo
                acciones.append(accion)
            if (fila_pirata-1)>=0:
                accion = (pirata,(-1,0)) #  Mover arriba
                acciones.append(accion)
            if (columna_pirata+1)<= columnas_tablero:
                accion = (pirata,(0,1)) #  Mover a la derecha
                acciones.append(accion)
            if (columna_pirata-1)>=0:
                accion = (pirata,(0,-1)) #  Mover a la izquierda
                acciones.append(accion)
        return acciones

    def cost(self, state,action,state2):
        return 1

    def result(self, state, action):
        franceses = tupla_a_lista(state[0])
        piratas = tupla_a_lista(state[1])

        fila_pirata, columna_pirata = action[0][0]
        fila_mover, columna_mover = action[1]
        fila_destino = fila_pirata + fila_mover
        columna_destino = columna_pirata + columna_mover

        frances_eliminar = ()
        pirata_eliminar = ()
        for pirata in piratas:
            if pirata[0]==(fila_pirata,columna_pirata): #Encontramos al pirata de la accion.
                for frances in franceses:
                    if frances == (fila_destino,columna_destino): # Si se encuentra con un frances, se eliminan los dos.
                        frances_eliminar=frances
                        pirata_eliminar=pirata
                        break
                if (fila_destino==isla_mapa[0]) and (columna_destino==isla_mapa[1]): #  Si encontro el mapa 
                    pirata[1] = 1
                    pirata[0]=(fila_destino,columna_destino)
                    break
                else:
                    pirata[0]=(fila_destino,columna_destino) #  Mover normalmente
                    break

        tupla_franceses = lista_a_tupla(franceses)
        tupla_piratas = lista_a_tupla(piratas) 

        estado = (tupla_franceses,tupla_piratas)

        return estado

    def heuristic(self, state):
        
        franceses = tupla_a_lista(state[0])
        piratas = tupla_a_lista(state[1])
        lista_distancias_mapa = []
        lista_distancias_sin_mapa = []

        #Si algun pirata tiene el mapa, la heuristica es la distancia desde su posicion a la isla pirata.
        for pirata in piratas:
            fila_pirata, columna_pirata = pirata[0]
            if pirata[1] == 1:
                lista_distancias_mapa.append(abs(filas_tablero-fila_pirata)+abs(columnas_tablero-columna_pirata))
            else:
                #Si ninguno tiene el mapa, la heuristica es la distancia mas cercana desde su posicion a la isla
                # del mapa + la distancia de isla a isla.
                lista_distancias_sin_mapa.append((abs(isla_mapa[0]-fila_pirata)+abs(isla_mapa[1]-columna_pirata))+(abs(filas_tablero-isla_mapa[0]+abs(columnas_tablero-isla_mapa[1]))))
        
        if lista_distancias_mapa != []:
            return min(lista_distancias_mapa)
        else:
            return min(lista_distancias_sin_mapa)

def Estado_Inicial(franceses, piratas):

    inicial = [[],[]]
    
    #  Agregar franceses y piratas a Inicial
    for frances in franceses:
        inicial[0].append(frances)
    for pirata in piratas:
        nuevopirata = (pirata,0)
        inicial[1].append(nuevopirata)
    
    return inicial

def resolver(metodo_busqueda, franceses,piratas):
    INICIAL=Estado_Inicial(franceses,piratas)

    # visor = ConsoleViewer()
    # visor = WebViewer()
    visor = None

    problema= ProblemaPiratas(lista_a_tupla(INICIAL))

    funciones = {
        'breadth_first': breadth_first,
        'depth_first': depth_first,
        'greedy': greedy,
        'astar': astar
    }
    funcion_busqueda = funciones[metodo_busqueda]

    resultado = funcion_busqueda(problema, graph_search=True,viewer=visor)

    # print("Nodos Visitados: {}".format(visor.stats['visited_nodes']))
    # print("Profundidad solucion: {}".format(len(resultado.path())))
    # print("Costo: {}".format(resultado.cost))
    # print("Tamaño máximo frontera: {}".format(visor.stats['max_fringe_size']))
    # print ("Resultado: ",resultado)
    # print ("Resultado path:",resultado.path)

    return resultado

# if __name__ == '__main__':

    # visor = ConsoleViewer()
    # franceses =  [(0, 2),(0, 3),(1, 2),(1, 3),(2, 1),(2, 2),(2, 3),(3, 0),(3, 1),(3, 2),(4, 0),(4, 1),(5, 0)]
    # piratas = [(4,4), (4,5), (5,4)]

    #result = resolver('breadth_first',franceses,piratas)
    #result = resolver('depth_first',franceses,piratas)
    #result = resolver('greedy',franceses,piratas)
    #result = resolver('astar',franceses,piratas)