import itertools
from simpleai.search import (CspProblem, backtrack, min_conflicts,MOST_CONSTRAINED_VARIABLE,LEAST_CONSTRAINING_VALUE,HIGHEST_DEGREE_VARIABLE)

def ProblemaCharlas():

    #Charlas
    variables = ['DjangoGirls','IntroduccionPython','KeynoteDiversidad','KeynotePython','APIsDjango','DiseñoSistemas',
    'UnitTestingPython','EditoresPython','MusicaPython','VendedorSoftware','AnalisisImagenesPython',
    'SatelitesEspaciales','LibPyPI','Pandas' ]    

    # AULAS: 1:Magna - 2:Aula42 - 3:Laboratorio
    aulas_permitidas = {
    'DjangoGirls': [3],
    'IntroduccionPython': [3],
    'KeynoteDiversidad': [1],
    'KeynotePython': [1],
    'APIsDjango': [1,2],
    'DiseñoSistemas': [1,3],
    'UnitTestingPython': [1,2],
    'EditoresPython': [1],
    'MusicaPython': [1,2],
    'VendedorSoftware': [2,3],
    'AnalisisImagenesPython': [1,2],
    'SatelitesEspaciales': [1,2],
    'LibPyPI': [1,2],
    'Pandas': [2]
    }

    horarios = [10,11,14,15,16,17]
    horarios_mañana=[10,11]
    horarios_tarde=[14,15,16,17]
    
    dominios ={}

    #  Agregar al dominio todas las charlas
    for charla,aulas in aulas_permitidas.items():
        dominios[charla] = []

    #  Agregar los horarios en el que se pueden dar las charlas
    for charla, aulas in aulas_permitidas.items():
        lista=[]
        for aula in aulas:
            if charla in dominios: 
                if charla == 'IntroduccionPython' or charla=='VendedorSoftware': #  Si es por la mañana
                    for horario in horarios_mañana:
                        charlaagregar=aula,horario
                        dominios[charla].append(charlaagregar)
                elif charla=='KeynoteDiversidad' or charla=='KeynotePython' or charla=='SatelitesEspaciales': #  Si es por la tarde
                    for horario in horarios_tarde:
                        charlaagregar=aula,horario
                        dominios[charla].append(charlaagregar)
                else:
                    for horario in horarios: #  Todos los horarios disponibles
                        charlaagregar=aula,horario
                        dominios[charla].append(charlaagregar)

    restricciones=[]
   
    for charla1,charla2 in itertools.combinations(variables,2):
        restricciones.append(((charla1,charla2),diferentes))

    #  Restringir si la charla es KeynotePython o KeynoteDiversidad para que ninguna mas pueda estar en ese horario
    for variable in variables:
        if variable != variables[2]:
            restricciones.append(((variables[2],variable),solamente_esa_charla))
        if variable != variables[3]:
            restricciones.append(((variables[3],variable),solamente_esa_charla))

    return CspProblem(variables, dominios, restricciones)

def diferentes(vars, vals):
    return vals[0]!=vals[1]

def solamente_esa_charla(vars, vals):
	return vals[0][1] != vals[1][1]

def resolver(metodo_busqueda,iteraciones):

    problema = ProblemaCharlas()

    if metodo_busqueda == "backtrack":
        resultado = backtrack(problema)
     
    elif metodo_busqueda == "min_conflicts":    
        resultado = min_conflicts(problema, iterations_limit=iteraciones)

    return resultado

# if __name__ == '__main__':
#     #resultado = resolver('backtrack', None)
#     resultado = resolver('min_conflicts', 50)

#     print(resultado)
#     #print(repr(resultado))
    
#     problema = ProblemaCharlas()